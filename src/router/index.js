import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [{
    path: '/',
    name: 'home',
    meta: {
      title: '首页'
    },
    component: () => import('../views/Home.vue')
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import( /* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/score',
    name: 'score',
    meta: {
      title: '成绩'
    },
    component: () => import('../views/Score.vue')
  }, {
    path: '/timetable',
    name: 'timetable',
    meta: {
      title: '课程表'
    },
    component: () => import('../views/Timetable.vue')
  }, {
    path: '/courses',
    name: 'courses',
    meta: {
      title: '课程'
    },
    component: () => import('../views/Courses.vue')
  }, {
    path: '/course/:course_id',
    name: 'course',
    meta: {
      title: '课程'
    },
    component: () => import('../views/Course.vue')
  }, {
    path: '/login',
    name: 'login',
    meta: {
      title: '登录'
    },
    component: () => import('../views/Login.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

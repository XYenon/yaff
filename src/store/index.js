import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    logged: false,
    token: "",
    stu_id: "",
    name: "",
    admin: false
  },
  mutations: {
    login(state, token) {
      state.token = token;
      localStorage.setItem("logged", true);
      localStorage.setItem("token", state.token);
    },
    setUser(state, user) {
      state.logged = true;
      state.stu_id = user.stu_id;
      state.name = user.name;
      state.admin = user.admin;
      localStorage.setItem("stu_id", state.stu_id);
      localStorage.setItem("name", state.name);
      localStorage.setItem("admin", state.admin);
    },
    loadInfo(state) {
      state.logged = true;
      state.token = localStorage.getItem("token");
      state.stu_id = localStorage.getItem("stu_id");
      state.name = localStorage.getItem("name");
      state.admin = localStorage.getItem("admin") === "true";
    },
    logout(state) {
      state.logged = false;
      state.token = "";
      state.stu_id = "";
      state.name = "";
      state.admin = false;
      localStorage.clear();
    }
  },
  actions: {},
  modules: {}
})

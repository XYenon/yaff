import {
  Bar
} from "vue-chartjs";

export default {
  extends: Bar,
  name: "CourseBarChart",
  props: ["myData"],
  data: function () {
    return {
      chartData: {
        labels: this.myData.labels,
        datasets: [{
          backgroundColor: "rgba(54, 162, 235, 0.6)",
          data: this.myData.data
        }]
      },
      options: {
        legend: {
          display: false
        }
      },
    };
  },
  mounted() {
    this.renderChart(this.chartData, this.options);
  }
};
